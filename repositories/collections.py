from datetime import datetime
from typing import List

from fastapi import HTTPException, status

from db.characters_collections import characters_collections
from models.collection import Collection

from .base import BaseRepository


class CollectionRepository(BaseRepository):
    async def create(self, path: str, date: datetime) -> Collection:
        characters_collection = Collection(id=0, path=path, date=date)
        values = {**characters_collection.dict()}
        values.pop("id", None)
        query = characters_collections.insert().values(**values)
        characters_collection.id = await self.database.execute(query=query)
        return characters_collection

    async def get_all(self, limit: int = 100, skip: int = 0) -> List[Collection]:
        query = characters_collections.select().limit(limit).offset(skip)
        return await self.database.fetch_all(query=query)

    async def get_by_id(self, id: int) -> Collection:
        query = characters_collections.select().where(characters_collections.c.id == id)
        characters_collection = await self.database.fetch_one(query=query)
        if characters_collection is None or characters_collection.id != id:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Collection not found")
        return Collection.parse_obj(characters_collection)
