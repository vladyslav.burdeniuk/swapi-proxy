import datetime

import sqlalchemy

from db.base import metadata

characters_collections = sqlalchemy.Table(
    "characters_collections",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True, autoincrement=True, unique=True),
    sqlalchemy.Column("path", sqlalchemy.String),
    sqlalchemy.Column("date", sqlalchemy.DateTime, default=datetime.datetime.utcnow),
)
