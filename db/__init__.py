from db.base import engine, metadata

metadata.create_all(bind=engine)
