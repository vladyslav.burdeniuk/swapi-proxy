from typing import List

import uvicorn
from fastapi import Depends, FastAPI

import core.async_api_fetcher
from core.depends import get_collection_repository
from core.etl import petl_agregate, petl_read
from core.settings import settings
from db.base import database
from models.character import Character
from models.collection import Collection
from models.fields_filter import FieldsFilter
from repositories.collections import CollectionRepository

app = FastAPI()


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


@app.get("/characters", response_model=List[Collection])
async def get_characters_collections(
    limit: int = 100,
    skip: int = 0,
    characters_collections: CollectionRepository = Depends(get_collection_repository),
):
    return await characters_collections.get_all(limit=limit, skip=skip)


@app.get("/characters/{collection_id}", response_model=List[Character])
async def get_characters_from_collection(
    collection_id: int,
    start: int = 0,
    end: int = 10,
    collections: CollectionRepository = Depends(get_collection_repository),
):
    collection = await collections.get_by_id(collection_id)

    return list(petl_read(collection.path, start, end))


@app.post("/characters/agregate/{collection_id}")  # , response_model=List[Character]
async def get_agregated_characters(
    collection_id: int,
    agregate_by: FieldsFilter,
    collections: CollectionRepository = Depends(get_collection_repository),
):

    collection = await collections.get_by_id(collection_id)

    return list(petl_agregate(collection.path, agregate_by))


@app.get("/get_fresh_characters", response_model=Collection)
async def get_fresh_characters(
    collections: CollectionRepository = Depends(get_collection_repository),
):
    path, date = await core.async_api_fetcher.main()
    return await collections.create(path=path, date=date)


if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host=settings.server_host,
        port=settings.server_port,
        reload=True,
    )
