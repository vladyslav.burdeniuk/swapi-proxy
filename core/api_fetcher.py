import csv
import json
import locale
import os.path
from datetime import datetime

import requests

from core.settings import settings
from models.character import Character
from models.planet import Planet

locale.setlocale(locale.LC_ALL, settings.source_api_locale)


class CharactersDictPrepare:
    """Normalize and convert some characters fields"""

    def __init__(self):
        self.planets_dict = get_planets_dict()
        self.date = datetime.now().strftime("%Y-%m-%d")

    # @staticmethod
    # def normalize_int(field):
    #     if field == "unknown":
    #         field = None
    #     else:
    #         field = locale.atoi(field)
    #     return field

    # @staticmethod
    # def normalize_float(field):
    #     if field == "unknown":
    #         field = None
    #     else:
    #         field = locale.atof(field)
    #     return field

    def convert_homeworld(self, field):
        return self.planets_dict[field]

    def prepare(self, character_dict):
        # character_dict["mass"] = self.normalize_float(character_dict["mass"])
        # character_dict["height"] = self.normalize_int(character_dict["height"])
        character_dict["homeworld"] = self.convert_homeworld(character_dict["homeworld"])
        character_dict["date"] = self.date
        return character_dict


class ApiFetcher:
    """Fetch API data, map to model, filter via item_prepare_function.
    Use fetch_items() to get result generator"""

    def __init__(self, url, model, item_prepare_cls=None):
        self.url = url
        self.model = model
        if item_prepare_cls:
            self.item_prepare = item_prepare_cls()

    @staticmethod
    def get_page(url: str):
        """Get page"""
        r = requests.get(url)
        if r.status_code == 200:
            return json.loads(r.text)

    def get_all_pages(self) -> str:
        """Walk thru all api pages"""
        response = {"next": self.url}

        while "next" in response and response["next"]:
            response = self.get_page(response["next"])
            yield response["results"]

    def parse_item(self, item: str) -> dict:
        """Parse item via model to dict"""
        if hasattr(self, "item_prepare"):
            item = self.item_prepare.prepare(item)

        return self.model(**item).dict()

    def parse_items(self, items: str):
        """Parse items"""
        for item in items:
            yield self.parse_item(item)

    def fetch_items(self):
        """Fetch items from all pages, return generator"""
        for page in self.get_all_pages():
            for item in self.parse_items(page):
                yield item

    def fetch_to_csv(self, file_path):
        characters_generator = self.fetch_items()
        self.save_to_csv(file_path, characters_generator)

    @staticmethod
    def save_to_csv(path, generator):
        with open(path, "w", newline="") as csvfile:
            first_line = next(generator)
            fieldnames = first_line.keys()

            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerow(first_line)

            for item_dict in generator:
                writer.writerow(item_dict)


def get_planets_dict():
    """Get all planets_dict from cached csv or fetch from API"""
    planets_dict = {}

    # fetch if cached csv doesn't exists
    if not os.path.isfile(settings.planets_csv_temp_file):
        ApiFetcher(settings.planet_url, Planet).fetch_to_csv(settings.planets_csv_temp_file)

    for planet_dict in csv.DictReader(open(settings.planets_csv_temp_file)):
        planet = Planet(**planet_dict)
        planets_dict[planet.url] = planet.name

    return planets_dict


async def characters_to_csv() -> tuple:
    date = datetime.now()
    now_formatted = date.strftime("%Y%m%d-%H%M%S")
    file_path = f"{settings.base_dir}/{settings.character_csv_dir}/characters_{now_formatted}.csv"
    ApiFetcher(settings.character_url, Character, CharactersDictPrepare).fetch_to_csv(file_path)
    return file_path, date


if __name__ == "__main__":
    print(characters_to_csv())
