from db.base import database
from repositories.collections import CollectionRepository


def get_collection_repository() -> CollectionRepository:
    return CollectionRepository(database)
