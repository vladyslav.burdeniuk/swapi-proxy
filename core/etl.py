import petl

from models.fields_filter import FieldsFilter


def petl_read(path: str, start: int = 0, end: int = 10):
    tbl = petl.fromcsv(path)
    tbl = petl.rowslice(tbl, start, end)
    tbl = petl.dicts(tbl)
    return tbl


def petl_agregate(path: str, agregate_by: FieldsFilter):
    tbl = petl.fromcsv(path)
    filter_dict = {key: value for (key, value) in agregate_by.dict().items() if value}
    tbl = petl.aggregate(tbl, key=list(filter_dict.keys()), aggregation=len)
    tbl = petl.dicts(tbl)
    return tbl
