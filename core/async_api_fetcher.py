import asyncio
import csv
import locale
import math
import os.path
from datetime import datetime

import aiohttp

from core.settings import settings
from models.api_response import CharacterApiResponse, PlanetApiResponse
from models.planet import Planet

locale.setlocale(locale.LC_ALL, settings.source_api_locale)


class CharacterPrepare:
    """Normalize and convert some characters fields"""

    def __init__(self, planets_dict):
        self.planets_dict = planets_dict

    def convert_homeworld(self, field):
        return self.planets_dict[field]

    def prepare(self, character):
        character.homeworld = self.convert_homeworld(character.homeworld)
        return character


class ApiFetcher:
    def __init__(self, url, model, file_path=None, item_prepare=None):
        self.url = url
        self.model = model
        if file_path:
            self.file_path = file_path
        else:
            self.file_path = self.make_file_path()
        if item_prepare:
            self.item_prepare = item_prepare

    def make_file_path(self):
        now_formatted = datetime.now().strftime("%Y%m%d-%H%M%S")
        return f"{settings.base_dir}/{settings.character_csv_dir}/characters_{now_formatted}.csv"

    async def get_all_pages(self) -> str:
        tasks = []
        first_page = await self.get_page(self.url)
        items_count = first_page["count"]
        items_per_page = len(first_page["results"])
        pages = math.ceil(items_count / items_per_page)
        for page in range(2, pages + 1):
            task = asyncio.ensure_future(self.get_page(self.url, {"page": page}))
            tasks.append(task)
        await asyncio.gather(*tasks)

    async def get_page(self, url: str, params=None):
        async with aiohttp.ClientSession() as session:
            async with session.get(url, params=params) as resp:
                res = await resp.json()
                api_repsonse = self.model(**res)
                items = await self.prepare_items(api_repsonse.results)
                self.save_to_csv(items)
                return res

    async def prepare_items(self, items: str) -> dict:
        if hasattr(self, "item_prepare"):
            for i, item in enumerate(items):
                items[i] = self.item_prepare.prepare(item)

        return items

    def save_to_csv(self, items):
        first_line = items[0]
        fieldnames = first_line.dict().keys()

        if not os.path.exists(self.file_path):
            with open(self.file_path, "w", newline="") as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()

        with open(self.file_path, "a", newline="") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            for item in items:
                writer.writerow(item.dict())


async def get_planets_dict():
    planets_dict = {}

    if not os.path.isfile(settings.planets_csv_temp_file):
        await ApiFetcher(
            url=settings.planet_url, model=PlanetApiResponse, file_path=settings.planets_csv_temp_file
        ).get_all_pages()

    for planet_dict in csv.DictReader(open(settings.planets_csv_temp_file)):
        planet = Planet(**planet_dict)
        planets_dict[planet.url] = planet.name

    return planets_dict


async def main():
    planets_dict = await get_planets_dict()
    characters_dict_prepare = CharacterPrepare(planets_dict=planets_dict)
    api_fetcher = ApiFetcher(url=settings.character_url, model=CharacterApiResponse, item_prepare=characters_dict_prepare)
    await api_fetcher.get_all_pages()

    return api_fetcher.file_path, datetime.now()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    file_path, date = loop.run_until_complete(main())
    print(file_path, date)
    loop.close()
