from datetime import datetime

from pydantic import BaseModel


class BaseCollection(BaseModel):
    path: str
    date: datetime


class Collection(BaseCollection):
    id: int
