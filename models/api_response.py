from typing import Optional

from pydantic import BaseModel

from .character import Character
from .planet import Planet


class ApiResponse(BaseModel):
    count: int
    next: Optional[str]
    previous: Optional[str]
    results: list


class CharacterApiResponse(ApiResponse):
    count: int
    next: Optional[str]
    previous: Optional[str]
    results: list[Character]


class PlanetApiResponse(ApiResponse):
    count: int
    next: Optional[str]
    previous: Optional[str]
    results: list[Planet]
