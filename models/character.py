from datetime import datetime

from pydantic import BaseModel


class Character(BaseModel):
    name: str
    hair_color: str
    skin_color: str
    eye_color: str
    birth_year: str
    gender: str
    homeworld: str
    height: str
    mass: str
    date: str = datetime.now().strftime("%Y-%m-%d")
