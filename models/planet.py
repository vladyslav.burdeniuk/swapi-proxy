from pydantic import BaseModel


class Planet(BaseModel):
    url: str
    name: str
