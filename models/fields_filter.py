from pydantic import BaseModel


class FieldsFilter(BaseModel):
    name: bool = False
    hair_color: bool = True
    skin_color: bool = True
    eye_color: bool = False
    birth_year: bool = False
    gender: bool = False
    homeworld: bool = False
    height: bool = False
    mass: bool = False
    date: bool = False
