fastapi
uvicorn
pydantic
pydantic[dotenv]
python-dotenv
sqlalchemy
requests
petl